package abillingham.haemohelper;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import abillingham.haemohelper.injury.AddInjuryController;
import abillingham.haemohelper.injury.DBInjuryCRUD;
import abillingham.haemohelper.injury_export.InjuryLogBackup;
import abillingham.haemohelper.injury.InjuryLogController;
import abillingham.haemohelper.reminder.ReminderController;

public class MainActivity extends AppCompatActivity {

    TextView txtStock;
    int stockValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_home);

        txtStock = findViewById(R.id.txtStockNumber);
        getStock();
        updateInjuryList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getStock();
        updateInjuryList();
    }

    public void updateInjuryList() {
        DBInjuryCRUD crud = new DBInjuryCRUD(this);
        ArrayList<HashMap<String, String>> injuryList = crud.getInjuryHash();
        ListView lv = findViewById(R.id.lstInjuries);

        if (injuryList.size() != 0) {

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    navInjury();
                }
            });

            ListAdapter adapter = new SimpleAdapter(this,
                    injuryList,
                    R.layout.viewinjury_simple,
                    new String[]{"id", "description", "cause", "time", "date", "locationOnBody"},
                    new int[]{R.id.txtID, R.id.txtDesc, R.id.txtCause, R.id.txtTime, R.id.txtDate, R.id.txtLocation});

            lv.setAdapter(adapter);

        } else {
            // If there are no meetings to show, display an error
            ArrayList<String> noInjuries = new ArrayList<>();
            noInjuries.add("No injuries to display");
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, noInjuries);
            lv.setAdapter(adapter);
        }
    }

    public void getStock() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(getString(R.string.stock), 0);
        stockValue = preferences.getInt(getString(R.string.stock), 0);
        txtStock.setText(String.valueOf(stockValue));
    }

    public void editStock(View view) {
        final Dialog dia = new Dialog(this);
        dia.setTitle("Number of Treatments");
        dia.setContentView(R.layout.number_picker);

        final NumberPicker np = dia.findViewById(R.id.numpickStock);
        Button btnSave = dia.findViewById(R.id.btnSave);
        Button btnCancel = dia.findViewById(R.id.btnCancel);

        np.setMinValue(0);
        np.setMaxValue(100);
        np.setWrapSelectorWheel(false);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stockValue = np.getValue();
                saveStock();
                dia.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dia.dismiss();
            }
        });

        dia.show();
    }

    public void saveStock() {
        txtStock.setText(String.valueOf(stockValue));

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(getString(R.string.stock), 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.stock), stockValue);
        editor.apply();
    }

    public void decrementStock(View view) {
        if(stockValue > 0) {
            stockValue -= 1;
        } else {
            Toast.makeText(this, "Error: Cannot have negative stock", Toast.LENGTH_SHORT).show();
        }
        saveStock();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    // Do nothing
                    return true;
                case R.id.navigation_injuryLog:
                    navInjury();
                    return true;
                case R.id.navigation_reminder:
                    navReminder();
                    return true;
                case R.id.navigation_injurylogbackup:
                    navInjuryBackup();
                    return true;
            }
            return false;
        }
    };

    public void navInjury() {
        Intent i = new Intent(this, InjuryLogController.class);
        startActivity(i);
    }

    public void navAddInjury(View view) {
        Intent i = new Intent(this, AddInjuryController.class);
        startActivity(i);
    }

    public void navReminder() {
        Intent i = new Intent(this, ReminderController.class);
        startActivity(i);
    }

    public void navInjuryBackup() {
        Intent i = new Intent(this, InjuryLogBackup.class);
        startActivity(i);
    }

}

package abillingham.haemohelper.injury;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import abillingham.haemohelper.DatePickerFragment;
import abillingham.haemohelper.MainActivity;
import abillingham.haemohelper.R;
import abillingham.haemohelper.TimePickerFragment;

public class AddInjuryController extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText inDescription;
    Spinner spinnerCause;
    Spinner spinnerLocation;
    Spinner spinnerLeftRight;
    TextView inTime;
    TextView inDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addinjury);

        inDescription = findViewById(R.id.editDesc);
        spinnerCause = findViewById(R.id.spinnerCause);
        spinnerLocation = findViewById(R.id.spinnerLocation);
        spinnerLeftRight = findViewById(R.id.spinnerLocationLR);
        inTime = findViewById(R.id.editTime);
        inDate = findViewById(R.id.editDate);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cause_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCause.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.location_array, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocation.setAdapter(adapter2);
        spinnerLocation.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                R.array.leftright_array, android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLeftRight.setAdapter(adapter3);
    }

    public void editTime(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void editDate(View view) {
        DialogFragment newFragment2 = new DatePickerFragment();
        newFragment2.show(getSupportFragmentManager(), "datePicker");
    }

    public void submitForm(View view) {
        Injury newInjury = new Injury();
        DBInjuryCRUD crud = new DBInjuryCRUD(this);

        if (inDescription.getText().toString().equals("")) {
            Toast.makeText(this, R.string.errorDesc, Toast.LENGTH_SHORT).show();
        } else {
            newInjury.description = inDescription.getText().toString();
        }

        newInjury.cause = spinnerCause.getSelectedItem().toString();

        if (spinnerLeftRight.isShown()) {
            newInjury.location = spinnerLocation.getSelectedItem().toString() + "(" + spinnerLeftRight.getSelectedItem().toString() + ")";
        } else {
            newInjury.location = spinnerLocation.getSelectedItem().toString();
        }

        if ((inTime.getText().toString()).equals("Click to edit time")) {
            Toast.makeText(this, R.string.errorTime, Toast.LENGTH_SHORT).show();
            return;
        } else {
            newInjury.bleedTime = inTime.getText().toString();
        }

        if ((inDate.getText().toString()).equals("Click to edit date")) {
            Toast.makeText(this, R.string.errorDate, Toast.LENGTH_SHORT).show();
            return;
        } else {
            newInjury.bleedDate = inDate.getText().toString();
        }

        crud.insert(newInjury);
        Toast.makeText(this, R.string.injuryAdded, Toast.LENGTH_SHORT).show();

        navHome();
    }

    public void navHome() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        switch (position) {
            case 0: // Head
                spinnerLeftRight.setVisibility(View.INVISIBLE);
                break;
            case 1: // Body
                spinnerLeftRight.setVisibility(View.INVISIBLE);
                break;
            case 2: // Arm
                spinnerLeftRight.setVisibility(View.VISIBLE);
                break;
            case 3: // Elbow
                spinnerLeftRight.setVisibility(View.VISIBLE);
                break;
            case 4: // Hand
                spinnerLeftRight.setVisibility(View.VISIBLE);
                break;
            case 5: // Leg
                spinnerLeftRight.setVisibility(View.VISIBLE);
                break;
            case 6: // Knee
                spinnerLeftRight.setVisibility(View.VISIBLE);
                break;
            case 7: // Foot
                spinnerLeftRight.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "No bleed cause selected", Toast.LENGTH_SHORT).show();
    }
}

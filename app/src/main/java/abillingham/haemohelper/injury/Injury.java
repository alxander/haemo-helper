package abillingham.haemohelper.injury;

public class Injury {

    // Table name
    public static final String TABLE = "Injury";

    // Table columns
    public static final String KEY_ID = "id";
    public static final String KEY_desc = "description";
    public static final String KEY_cause = "cause";
    public static final String KEY_location = "location";
    public static final String KEY_time = "time";
    public static final String KEY_date = "date";

    // Properties
    public int injuryID;
    public String description;
    public String cause;
    public String location;
    public String bleedTime;
    public String bleedDate;
}

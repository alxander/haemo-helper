package abillingham.haemohelper.injury;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DBInjuryCRUD {

    private DBInjuryManager dbInjuryManager;

    public DBInjuryCRUD(Context context) {
        dbInjuryManager = new DBInjuryManager(context);
    }

    /**
     * Inserts an Injury object into the database
     */
    public int insert(Injury injury) {
        // Open connection to write data
        SQLiteDatabase db = dbInjuryManager.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(Injury.KEY_desc, injury.description);
        values.put(Injury.KEY_cause, injury.cause);
        values.put(Injury.KEY_location, injury.location);
        values.put(Injury.KEY_time, injury.bleedTime);
        values.put(Injury.KEY_date, injury.bleedDate);

        // Inserting row into DB
        long injuryID = db.insert(Injury.TABLE, null, values);
        db.close();

        return (int) injuryID;
    }

    /**
     * Outputs all attendees into a hashmap
     */
    public ArrayList<HashMap<String, String>> getInjuryHash() {
        SQLiteDatabase db = dbInjuryManager.getReadableDatabase();

        String selectQuery = "SELECT "
                + Injury.KEY_ID + ", "
                + Injury.KEY_desc + ", "
                + Injury.KEY_cause + ", "
                + Injury.KEY_location + ", "
                + Injury.KEY_time + ", "
                + Injury.KEY_date
                + " FROM " + Injury.TABLE;

        ArrayList<HashMap<String, String>> injuryList = new ArrayList<>();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // Loop throw table rows + add to list
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> injury = new HashMap<>();
                injury.put("id", cursor.getString(cursor.getColumnIndex(Injury.KEY_ID)));
                injury.put("description", cursor.getString(cursor.getColumnIndex(Injury.KEY_desc)));
                injury.put("cause", cursor.getString(cursor.getColumnIndex(Injury.KEY_cause)));
                injury.put("location", cursor.getString(cursor.getColumnIndex(Injury.KEY_location)));
                injury.put("time", cursor.getString(cursor.getColumnIndex(Injury.KEY_time)));
                injury.put("date", cursor.getString(cursor.getColumnIndex(Injury.KEY_date)));
                injuryList.add(injury);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return injuryList;
    }

    public String getInjuryString() {
        SQLiteDatabase db = dbInjuryManager.getReadableDatabase();

        String selectQuery = "SELECT "
                + Injury.KEY_ID + ", "
                + Injury.KEY_desc + ", "
                + Injury.KEY_cause + ", "
                + Injury.KEY_location + ", "
                + Injury.KEY_time + ", "
                + Injury.KEY_date
                + " FROM " + Injury.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.UK);
        Date nowDateTime = new Date();
        dateFormat.format(nowDateTime);

        String output = "";

        if (cursor.moveToFirst()) {
            do {
                output += ("ID: " + cursor.getString(cursor.getColumnIndex(Injury.KEY_ID)) + "\n");
                output += ("Description: " + cursor.getString(cursor.getColumnIndex(Injury.KEY_desc)) + "\n");
                output += ("Cause: " + cursor.getString(cursor.getColumnIndex(Injury.KEY_cause)) + "\n");
                output += ("Location: " + cursor.getString(cursor.getColumnIndex(Injury.KEY_location)) + "\n");
                output += ("Time: " + cursor.getString(cursor.getColumnIndex(Injury.KEY_time)) + "\n");
                output += ("Date: " + cursor.getString(cursor.getColumnIndex(Injury.KEY_date)) + "\n");

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return (output);
    }

    /**
     * Updates an injury with a newer version of the object
     */
    public void update(Injury injury) {
        SQLiteDatabase db = dbInjuryManager.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Injury.KEY_desc, injury.description);
        values.put(Injury.KEY_cause, injury.cause);
        values.put(Injury.KEY_location, injury.location);
        values.put(Injury.KEY_time, injury.bleedTime);
        values.put(Injury.KEY_date, injury.bleedDate);

        db.update(Injury.TABLE, values, Injury.KEY_ID + "= ?", new String[]{String.valueOf(injury.injuryID)});
        db.close();
    }

    /**
     * Deletes an injury, given an id
     */
    public void delete(int injuryID) {
        SQLiteDatabase db = dbInjuryManager.getWritableDatabase();
        db.delete(Injury.TABLE, Injury.KEY_ID + "= ?", new String[]{String.valueOf(injuryID)});
        db.close();
    }


}

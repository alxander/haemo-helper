package abillingham.haemohelper.injury;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import abillingham.haemohelper.MainActivity;

public class DBInjuryManager extends SQLiteOpenHelper {
    //version number to upgrade database version (+1 for any updates)
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "injury.db";

    public DBInjuryManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here
        String CREATE_TABLE_INJURY = "CREATE TABLE " + Injury.TABLE + "("
                + Injury.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Injury.KEY_desc + " Text, "
                + Injury.KEY_cause + " Text, "
                + Injury.KEY_location + " Text, "
                + Injury.KEY_time + " Text, "
                + Injury.KEY_date + " Text )";

        db.execSQL(CREATE_TABLE_INJURY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Injury.TABLE);

        // Create tables again
        onCreate(db);
    }

    public static String getDBName(){
        return DATABASE_NAME;
    }
}

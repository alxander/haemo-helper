package abillingham.haemohelper.injury;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import abillingham.haemohelper.MainActivity;
import abillingham.haemohelper.R;
import abillingham.haemohelper.injury_export.InjuryLogBackup;
import abillingham.haemohelper.reminder.ReminderController;

public class InjuryLogController extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_injurylogger);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_injuryLog);

        updateList();
    }

    /**
     * Populates the meeting list with existing meetings from the meeting database
     */
    public void updateList() {
        DBInjuryCRUD crud = new DBInjuryCRUD(this);
        ArrayList<HashMap<String, String>> injuryList = crud.getInjuryHash();
        ListView lv = findViewById(R.id.lstInjuries);
        final Toast toast = Toast.makeText(this, R.string.longPressToDelete, Toast.LENGTH_SHORT);

        if (injuryList.size() != 0) {

            // Hint to the use that long press is to delete
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    toast.show();
                }
            });

            // Use a long click listener to allow the user to delete an attendee
            lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView txtID = view.findViewById(R.id.txtID);
                    int injuryID = Integer.parseInt(txtID.getText().toString());
                    deleteInjury(injuryID);
                    return true;
                }
            });

            ListAdapter adapter = new SimpleAdapter(this,
                    injuryList,
                    R.layout.viewinjury,
                    new String[]{"id", "description", "cause", "location", "time", "date",},
                    new int[]{R.id.txtID, R.id.txtDesc, R.id.txtCause, R.id.txtLocation, R.id.txtTime, R.id.txtDate});

            lv.setAdapter(adapter);

        } else {
            // If there are no meetings to show, display an error
            ArrayList<String> noInjuries = new ArrayList<>();
            String err = "No injuries added";
            noInjuries.add(err);
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, noInjuries);
            lv.setAdapter(adapter);
        }
    }

    public void deleteInjury(final int id) {
        final DBInjuryCRUD crud = new DBInjuryCRUD(this);
        final Toast toast = Toast.makeText(this, R.string.deleteInjury, Toast.LENGTH_SHORT);

        // Create a warning dialogue
        AlertDialog.Builder adBuilder = new AlertDialog.Builder(this);
        adBuilder
                .setTitle(R.string.delete)
                .setMessage(R.string.askDeleteInjury)
                // If user clicks yes, attendee is deleted
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        crud.delete(id);
                        toast.show();
                        updateList();
                    }
                })
                // If user clicks no, nothing happens
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addicon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                navAddInjury();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    navMain();
                    return true;
                case R.id.navigation_reminder:
                    navReminder();
                    return true;
                case R.id.navigation_injuryLog:
                    // Do nothing
                    return true;
                case R.id.navigation_injurylogbackup:
                    navInjuryBackup();
                    return true;
            }
            return false;
        }
    };

    public void navMain() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void navReminder() {
        Intent i = new Intent(this, ReminderController.class);
        startActivity(i);
    }

    public void navAddInjury() {
        Intent i = new Intent(this, AddInjuryController.class);
        startActivity(i);
    }

    public void navInjuryBackup() {
        Intent i = new Intent(this, InjuryLogBackup.class);
        startActivity(i);
    }
}
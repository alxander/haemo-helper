package abillingham.haemohelper.injury_export;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import abillingham.haemohelper.MainActivity;
import abillingham.haemohelper.R;
import abillingham.haemohelper.injury.DBInjuryCRUD;
import abillingham.haemohelper.injury.InjuryLogController;
import abillingham.haemohelper.reminder.ReminderController;

public class InjuryLogBackup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_injurylogbackup);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_injurylogbackup);
    }

    public void emailDB(View view) {
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm", Locale.UK);
        Date date = new Date(System.currentTimeMillis());

        DBInjuryCRUD crud = new DBInjuryCRUD(this);
        String dbOutput = formatter.format(date) + "\n";
        dbOutput += crud.getInjuryString();

        Intent sendIntent = new Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, dbOutput)
                .setType("text/plain");

        startActivity(sendIntent);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    navHome();
                    return true;
                case R.id.navigation_injuryLog:
                    navInjury();
                    return true;
                case R.id.navigation_reminder:
                    navReminder();
                    return true;
                case R.id.navigation_injurylogbackup:
                    // Do nothing
                    return true;
            }
            return false;
        }
    };

    public void navHome() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void navInjury() {
        Intent i = new Intent(this, InjuryLogController.class);
        startActivity(i);
    }

    public void navReminder() {
        Intent i = new Intent(this, ReminderController.class);
        startActivity(i);
    }
}

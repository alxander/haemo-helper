package abillingham.haemohelper.reminder;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Calendar;

import abillingham.haemohelper.MainActivity;
import abillingham.haemohelper.R;
import abillingham.haemohelper.injury_export.InjuryLogBackup;
import abillingham.haemohelper.injury.InjuryLogController;

public class ReminderController extends AppCompatActivity {

    private int pickHour;
    private int pickMinute;

    private EditText inInterval;
    private TextView inTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_reminder);

        inInterval = findViewById(R.id.editInterval);
        inInterval.setText("0");
        inTime = findViewById(R.id.editTime);
    }

    public void setReminder(View view) {
        int tempInterval;

        if (TextUtils.isEmpty(inInterval.getText().toString())) {
            tempInterval = 0;
        } else {
            tempInterval = Integer.valueOf(inInterval.getText().toString());
        }

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), pickHour, pickMinute);

        Calendar endTime = Calendar.getInstance();
        endTime.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), pickHour + 1, pickMinute);

        Intent newEvent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, "Treatment")
                .putExtra(CalendarContract.Events.DESCRIPTION, "<a href=\"https://www.google.com/url?q=http%3A%2F%2Falxander.xyz&amp;sa=D&amp;usd=2&amp;usg=AFQjCNFocxACbOmPoJ2BXvjFUVgVrzPscQ\" target=\"_blank\">Done your treatment?</a>")
                .putExtra(CalendarContract.Events.RRULE, "FREQ=DAILY;INTERVAL=" + tempInterval);

        startActivity(newEvent);
    }

    public void editTime(View view) {
        final Dialog dia = new Dialog(this);
        dia.setTitle("Reminder Time");
        dia.setContentView(R.layout.time_picker);

        final NumberPicker npHour = dia.findViewById(R.id.numpickHour);
        final NumberPicker npMinute = dia.findViewById(R.id.numpickMinute);
        final Button btnSave = dia.findViewById(R.id.btnSave);
        final Button btnCancel = dia.findViewById(R.id.btnCancel);

        npHour.setMinValue(0);
        npHour.setMaxValue(23);
        npHour.setWrapSelectorWheel(true);
        npHour.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });

        npMinute.setMinValue(0);
        npMinute.setMaxValue(60);
        npMinute.setWrapSelectorWheel(true);
        npMinute.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickHour = npHour.getValue();
                pickMinute = npMinute.getValue();
                inTime.setText(String.format("%02d", pickHour) + " : " + String.format("%02d", pickMinute));
//                String timeString = getString(R.string.timeFormat, pickHour, pickMinute);
//                inTime.setText(timeString);
                dia.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dia.dismiss();
            }
        });

        dia.show();
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    navMain();
                    return true;
                case R.id.navigation_injuryLog:
                    navInjury();
                    return true;
                case R.id.navigation_reminder:
                    // Do nothing
                    return true;
                case R.id.navigation_injurylogbackup:
                    navInjuryBackup();
                    return true;
            }
            return false;
        }
    };

    public void navMain() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void navInjury() {
        Intent i = new Intent(this, InjuryLogController.class);
        startActivity(i);
    }

    public void navInjuryBackup() {
        Intent i = new Intent(this, InjuryLogBackup.class);
        startActivity(i);
    }
}
